package net.tecgurus.demo.persistence.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import net.tecgurus.demo.persistence.entities.Product;

@RepositoryRestResource(
		collectionResourceRel = "product",
		path = "product")
public interface ProductDAO extends JpaRepository<Product, Integer> {

	
	
	
	
	
}
