package net.tecgurus.demo.persistence.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import net.tecgurus.demo.persistence.entities.User;

public interface UserDAO extends JpaRepository<User, Integer> {
	
	/*
	 * Encuentra usuarios por el nombre
	 * */
	List<User> findAllByName(String name);
	
	User findTopByEmailOrderByNameAsc(String email);
	
	
}
