package net.tecgurus.demo.controller.rest;

import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import net.tecgurus.demo.persistence.daos.UserDAO;
import net.tecgurus.demo.persistence.entities.User;

@RestController
@RequestMapping("/api/v1/users")
public class UserRestEndpoint {

	@Autowired
	private UserDAO userDao;
	
	private static final Logger log = 
			LoggerFactory.
			getLogger(UserRestEndpoint.class);
	
	@PostConstruct
	private void init() {
		User user1 = new User();
		user1.setEmail("amaro.coria@gmail.com");
		user1.setName("jorge");
		user1.setSurname("amaro");
		userDao.save(user1);
	}
	
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public List<User> fetchUsers(){
		return userDao.findAll();
	}
	
	@RequestMapping(value = "/users", method = RequestMethod.POST)
	public User saveUser(@RequestBody User userRequest) {
		return userDao.save(userRequest);
	}
	
	
	/*
	 * Filtro por nombre
	 * */
	@RequestMapping(value = "/users/{name}", 
			method=RequestMethod.GET)
	//@GetMapping
	public List<User> findByName(@PathVariable String name){
		try {
			List<User> listUsers = 
					userDao.findAllByName(name);
			if(listUsers == null ||
					listUsers.isEmpty()) {
				log.debug("Aqui va a ir un 404");
				return null;
			}
			return listUsers;
		}catch(Exception e) {
			log.error("Aqui va a ir un 500");
			log.error("Error message: {}", e.getMessage());
			return null;
		}
	}
	
	@RequestMapping(value = "/users/email",
			method = RequestMethod.POST)
	public User findByEmail(@RequestBody String email) {
		try {
			User user = userDao
					.findTopByEmailOrderByNameAsc(email);
			if(user == null) {
				//ALGO
				return null;
			}
			return user;
		}catch(Exception e) {
			return null;
		}
	}
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}