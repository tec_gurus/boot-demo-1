package net.tecgurus.demo.controller.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1")
public class RestControllerHolaMundo {
	
	@Value("${tec.gurus.label}")
	private String label;

	@GetMapping(value = "/saludo")
	//@RequestMapping(value = "/saludo", method = RequestMethod.GET)
	public String hello() {
		return "Hello world!. Label: "+label;
	}
	
	@PostMapping(value = "/saludo")
	//@RequestMapping(value = "/saludo", method = RequestMethod.POST)
	public String saluda(@RequestBody String nombre) {
		return "Hola: "+nombre;
	}
	
}